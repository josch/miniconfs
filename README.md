The debconf-share repositories are a way to ensure files related to
DebConf are preserved in one place, and to share them with other
attendees.

This includes photos (including raw images), videos, talk slides,
documents, GPS tracks, and so on.

Please limit uploads to content which is legal, respectful of others,
and appropriate to share with the world.

We do not have infinite amounts of disk space, so please use common
sense before uploading huge files.

## Organization

This repository contains content from multiple miniconfs, please create
a top level directory for each miniconf, named `YEAR-MINICONF`, e.g.
`2024-minidebconf-berlin`.

## Uploading slides

If you are uploading slides or some other kind of visual support, please make
sure to upload them in a ['slides'][slides] sub-directory and to name them
properly.

Your file (or your directory) should be named after your talk's URL slug on
the miniconf's website, or your talk's video URL in the video archive.

For example, the web page for the [FIXME][fixme] is:
`https://fixme.mini.debconf.org/talks/fixme/`.
As such, the slides for this talk were uploaded in the
`slides/fixme/`
directory.

[slides]: https://salsa.debian.org/debconf-team/public/share/miniconfs/-/tree/main/fixme/slides
[fixme]: https://fixme.mini.debconf.org/talks/fixme/

## Web

All files in the repository can be accessed via the web:
<https://salsa.debian.org/debconf-team/public/share/miniconfs>

## Setup

All Debian developers (DDs) can directly push to branches in this
repository.
Others can fork the repository or ask any DD for commit rights.

The main branch is protected behind CI, to ensure that LFS is used (see
below).
Instead of pushing directly to main, you'll need to push to another
branch, file a merge request, and merge it once CI has passed.

The repository uses git LFS (git large file system).
This makes handling of big files more efficient, but requires some
additional attention when adding files to the repository.

1. You need the package `git-lfs`.
1. On Debian systems `# apt install git-lfs` (Buster and later).
1. Run `git lfs install` to set up the "lfs" filter in your system-wide
   git config.

Using `git clone` by itself will download all the large git LFS files
inside the repo, which will be many gigabytes.

Instead, you can do a shallow clone:

1. `$ GIT_LFS_SKIP_SMUDGE=1 git clone --config filter.lfs.smudge=true  git@salsa.debian.org:debconf-team/public/share/miniconfs.git`
1. `$ cd miniconfs`
1. Using smudge causes files to only be stored as references.
   To actually fetch a file, run `git lfs fetch -I <filename>;git lfs checkout <filename>`.
1. Use the repo as though it were a normal git repository.
   To add files, see below.

## Adding files

Git directs files to LFS, rather than the git repository, when the
filename matches a pattern in the `.gitattributes` file.
This repo is set up to store a number of common large files in LFS.

You can see the files already stored in LFS, by running
`git lfs ls-files`.

Before committing, run `git lfs status` to check that your large files
will be committed to LFS:

```
$ git checkout -b my-photos
$ git add DSC_0001.JPG
$ git add VIDEO_0001.MP4
$ git lfs status
On branch my-photos

Git LFS objects to be committed:

	DSC_0001.JPG (LFS: 0eb9017)
	VIDEO_0001.MP4 (Git: a9fe88d)

Git LFS objects not staged for commit:
```

In the above example, `DSC_0001.JPG` will be stored in LFS, but
`VIDEO_0001.MP4` will not.
To fix that, because it is reasonable to expect all MP4 files to be
stored in LFS:

```
$ git lfs track '*.MP4'
Tracking "*.MP4"
$ git add VIDEO_0001.MP4  # Re-adding it, this time to LFS
$ git add .gitattributes
$ git lfs status
On branch my-photos

Git LFS objects to be committed:

	.gitattributes (Git: b8183ad -> Git: e280251)
	DSC_0001.JPG (LFS: 0eb9017)
	VIDEO_0001.MP4 (LFS: a9fe88d)

Git LFS objects not staged for commit:

```

Then, commit and push as usual:

    $ git add slides/testfile.svg
    $ git commit -m"commit message"
    $ git push origin my-photos

Check the CI, file an MR, and merge it (or wait for someone else to).

## License

License for all files uploaded: CC-BY-SA 4.0 unless explicitly stated otherwise.
